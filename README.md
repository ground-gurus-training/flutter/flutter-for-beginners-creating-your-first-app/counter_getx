# Counter App using GetX

A simple counter app that uses GetX for state management

## GetX Package

[https://pub.dev/packages/get](https://pub.dev/packages/get)

[![Alt text](https://groundgurus-assets.s3.ap-southeast-1.amazonaws.com/Ground+Gurus+-+Logo+Small.png)](https://www.facebook.com/groups/1290693181003142)
